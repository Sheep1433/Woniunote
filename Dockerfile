FROM python:3.6.8
COPY . /usr/src/app
WORKDIR /usr/src/app
RUN pip install -r requirements.txt
RUN pip install gunicorn gevent

EXPOSE 80

CMD gunicorn  -c gun.conf app:app

